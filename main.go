package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"unicode/utf8"
)

/* structure of data to save two symbols */
type BiSymb struct {
	values [2]int32
}

/* structure of data to save three symbols */
type TriSymb struct {
	values [3]int32
}

const (
	maxNumberSymbols = 5000 /* max number of symbols in array */
)

/* gaps for Shell sort */
var (
	interval = [7]int{301, 132, 57, 23, 10, 4, 1}
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to calculate symbols in file */
func calculateSymbolsInFile(filename string) (map[int32]int, int, map[[2]int32]int, int, map[[3]int32]int, int) {

	/* declarations of variables */
	var (
		err        error
		reader     *bufio.Reader
		file       *os.File
		mapSymb    map[int32]int
		mapSymbBi  map[[2]int32]int
		mapSymbTri map[[3]int32]int
		length     int
		lengthBi   int
		lengthTri  int
		line       []byte
		row        string
		i          int
		width, w   int
		biValues   [2]int32
		triValues  [3]int32
	)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* create maps */
	mapSymb = make(map[int32]int)
	mapSymbBi = make(map[[2]int32]int)
	mapSymbTri = make(map[[3]int32]int)

	/* initial lengths */
	length = 0
	lengthBi = 0
	lengthTri = 0
	/* create reader and read file */
	reader = bufio.NewReader(file)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		/* handle row */
		for _, c := range row {
			/* check existence and add to counter */
			if _, ok := mapSymb[c]; ok {
				mapSymb[c]++
			} else {
				mapSymb[c] = 1
			}
			/* add one symbol to total length */
			length += 1
		}

		/* handle row (double symbols) */
		w = 0
		for i = 0; i < len(row); i += w {
			/* handle the first symbol */
			biValues[0], width = utf8.DecodeRuneInString(row[i:])
			i += width
			if i < len(row) {
				/* handle the second symbol */
				biValues[1], width = utf8.DecodeRuneInString(row[i:])
				w = width
				/* add new double symbol */
				if _, ok := mapSymbBi[biValues]; ok {
					mapSymbBi[biValues]++
				} else {
					mapSymbBi[biValues] = 1
				}
				/* add one double symbol to total length */
				lengthBi += 1
			} else {
				break
			}
		}

		/* handle row (triple symbols) */
		w = 0
		for i = 0; i < len(row); i += w {
			/* handle the first symbol */
			triValues[0], width = utf8.DecodeRuneInString(row[i:])
			i += width
			if i < len(row) {
				/* handle the second symbol */
				triValues[1], width = utf8.DecodeRuneInString(row[i:])
				i += width
			} else {
				break
			}
			if i < len(row) {
				/* handle the third symbol */
				triValues[2], width = utf8.DecodeRuneInString(row[i:])
				w = width
				/* add new triple symbol */
				if _, ok := mapSymbTri[triValues]; ok {
					mapSymbTri[triValues]++
				} else {
					mapSymbTri[triValues] = 1
				}
				/* add one triple symbol to total length */
				lengthTri += 1
			} else {
				break
			}
		}

	}

	/* close file */
	err = file.Close()
	handleError(err)

	/* return answer */
	return mapSymb, length, mapSymbBi, lengthBi, mapSymbTri, lengthTri

}

/* function to make statics */
func makeStatics(mapSymb map[int32]int, length int, mapSymbBi map[[2]int32]int, lengthBi int, mapSymbTri map[[3]int32]int, lengthTri int) ([]float64, []int, []int32, []float64, []int, []BiSymb, []float64, []int, []TriSymb) {

	/* declarations of variables */
	var (
		freq        []int
		prob        []float64
		symb        []int32
		freqBi      []int
		probBi      []float64
		symbBi      []BiSymb
		freqTri     []int
		probTri     []float64
		symbTri     []TriSymb
		i           int
		currSymb    int32
		currBiSymb  [2]int32
		emptBiSymb  BiSymb
		currTriSymb [3]int32
		emptTriSymb TriSymb
		currFreq    int
	)

	/* allocate arrays of single symbols */
	freq = make([]int, 0, maxNumberSymbols)
	prob = make([]float64, 0, maxNumberSymbols)
	symb = make([]int32, 0, maxNumberSymbols)

	/* fill arrays using map of single symbols */
	for currSymb, currFreq = range mapSymb {
		freq = append(freq, currFreq)
		prob = append(prob, 0.0)
		symb = append(symb, currSymb)
	}

	/* calculate probability of single symbols */
	i = 0
	for i < len(freq) {
		prob[i] = float64(freq[i]) / float64(length)
		i++
	}

	/* allocate arrays of double symbols */
	freqBi = make([]int, 0, maxNumberSymbols)
	probBi = make([]float64, 0, maxNumberSymbols)
	symbBi = make([]BiSymb, 0, maxNumberSymbols)

	/* fill arrays using map of double symbols */
	emptBiSymb.values[0] = 0
	emptBiSymb.values[1] = 0
	i = 0
	for currBiSymb, currFreq = range mapSymbBi {
		freqBi = append(freqBi, currFreq)
		probBi = append(probBi, 0.0)
		symbBi = append(symbBi, emptBiSymb)
		symbBi[i].values[0] = currBiSymb[0]
		symbBi[i].values[1] = currBiSymb[1]
		i++
	}

	/* calculate probability of double symbols */
	i = 0
	for i < len(freqBi) {
		probBi[i] = float64(freqBi[i]) / float64(lengthBi)
		i++
	}

	/* allocate arrays of triple symbols */
	freqTri = make([]int, 0, maxNumberSymbols)
	probTri = make([]float64, 0, maxNumberSymbols)
	symbTri = make([]TriSymb, 0, maxNumberSymbols)

	/* fill arrays using map of triple symbols */
	emptTriSymb.values[0] = 0
	emptTriSymb.values[1] = 0
	emptTriSymb.values[2] = 0
	i = 0
	for currTriSymb, currFreq = range mapSymbTri {
		freqTri = append(freqTri, currFreq)
		probTri = append(probTri, 0.0)
		symbTri = append(symbTri, emptTriSymb)
		symbTri[i].values[0] = currTriSymb[0]
		symbTri[i].values[1] = currTriSymb[1]
		symbTri[i].values[2] = currTriSymb[2]
		i++
	}

	/* calculate probability of triple symbols */
	i = 0
	for i < len(freqTri) {
		probTri[i] = float64(freqTri[i]) / float64(lengthTri)
		i++
	}

	/* return answer */
	return prob, freq, symb, probBi, freqBi, symbBi, probTri, freqTri, symbTri

}

/* do Shell sort alphabetically using single symbols */
func sortSymbols(symb *[]int32, freq *[]int, prob *[]float64) {

	var (
		i, i1, i2 int
		currSymb  int32
		currProb  float64
		currFreq  int
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(*prob) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				if (*symb)[i2] > (*symb)[i2+interval[i]] {
					currSymb = (*symb)[i2]
					(*symb)[i2] = (*symb)[i2+interval[i]]
					(*symb)[i2+interval[i]] = currSymb
					currProb = (*prob)[i2]
					(*prob)[i2] = (*prob)[i2+interval[i]]
					(*prob)[i2+interval[i]] = currProb
					currFreq = (*freq)[i2]
					(*freq)[i2] = (*freq)[i2+interval[i]]
					(*freq)[i2+interval[i]] = currFreq
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

}

/* do Shell sort by probability using single symbols */
func sortProbability(symb *[]int32, freq *[]int, prob *[]float64) {

	var (
		i, i1, i2 int
		currSymb  int32
		currProb  float64
		currFreq  int
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(*prob) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				if (*prob)[i2] < (*prob)[i2+interval[i]] {
					currSymb = (*symb)[i2]
					(*symb)[i2] = (*symb)[i2+interval[i]]
					(*symb)[i2+interval[i]] = currSymb
					currProb = (*prob)[i2]
					(*prob)[i2] = (*prob)[i2+interval[i]]
					(*prob)[i2+interval[i]] = currProb
					currFreq = (*freq)[i2]
					(*freq)[i2] = (*freq)[i2+interval[i]]
					(*freq)[i2+interval[i]] = currFreq
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

}

/* do Shell sort by probability using double symbols */
func sortDoubleProbability(symbBi *[]BiSymb, freqBi *[]int, probBi *[]float64) {

	var (
		i, i1, i2 int
		currSymb  BiSymb
		currProb  float64
		currFreq  int
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(*probBi) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				if (*probBi)[i2] < (*probBi)[i2+interval[i]] {
					currSymb = (*symbBi)[i2]
					(*symbBi)[i2] = (*symbBi)[i2+interval[i]]
					(*symbBi)[i2+interval[i]] = currSymb
					currProb = (*probBi)[i2]
					(*probBi)[i2] = (*probBi)[i2+interval[i]]
					(*probBi)[i2+interval[i]] = currProb
					currFreq = (*freqBi)[i2]
					(*freqBi)[i2] = (*freqBi)[i2+interval[i]]
					(*freqBi)[i2+interval[i]] = currFreq
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

}

/* do Shell sort by probability using triple symbols */
func sortTripleProbability(symbTri *[]TriSymb, freqTri *[]int, probTri *[]float64) {

	var (
		i, i1, i2 int
		currSymb  TriSymb
		currProb  float64
		currFreq  int
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(*probTri) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				if (*probTri)[i2] < (*probTri)[i2+interval[i]] {
					currSymb = (*symbTri)[i2]
					(*symbTri)[i2] = (*symbTri)[i2+interval[i]]
					(*symbTri)[i2+interval[i]] = currSymb
					currProb = (*probTri)[i2]
					(*probTri)[i2] = (*probTri)[i2+interval[i]]
					(*probTri)[i2+interval[i]] = currProb
					currFreq = (*freqTri)[i2]
					(*freqTri)[i2] = (*freqTri)[i2+interval[i]]
					(*freqTri)[i2+interval[i]] = currFreq
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

}

/* function to write results */
func writeResults(prob []float64, freq []int, symb []int32, length int, probBi []float64, freqBi []int, symbBi []BiSymb, lengthBi int,
	probTri []float64, freqTri []int, symbTri []TriSymb, lengthTri int) {

	/* declarations of variables */
	var (
		i int
	)

	/* write results */

	/* sort single symbols alphabetically */
	fmt.Println()
	sortSymbols(&symb, &freq, &prob)

	/* single value */
	fmt.Printf("Length of text: %d\n", length)

	/* table values */
	fmt.Println()
	fmt.Println("Single symbols that were sorted alphabetically:")
	i = 0
	for i < len(prob) {
		fmt.Printf("Probability: %f, Frequancy: %d, Symbol: %s\n", prob[i], freq[i], string(symb[i]))
		i++
	}

	/* sort single symbols by probability */
	sortProbability(&symb, &freq, &prob)

	/* table values */
	fmt.Println()
	fmt.Println("Single symbols that were sorted by probability:")
	i = 0
	for i < len(prob) {
		fmt.Printf("Probability: %f, Frequancy: %d, Symbol: %s\n", prob[i], freq[i], string(symb[i]))
		i++
	}

	/* single value */
	fmt.Println()
	fmt.Printf("Number of double symbols: %d\n", lengthBi)

	/* sort double symbols by probability */
	sortDoubleProbability(&symbBi, &freqBi, &probBi)

	/* table values */
	fmt.Println()
	fmt.Println("Double symbols that were sorted by probability:")
	i = 0
	for i < len(probBi) {
		fmt.Printf("Probability: %f, Frequancy: %d, Symbol: %c%c\n", probBi[i], freqBi[i], symbBi[i].values[0], symbBi[i].values[1])
		i++
	}

	/* single value */
	fmt.Println()
	fmt.Printf("Number of triple symbols: %d\n", lengthTri)

	/* sort triple symbols by probability */
	sortTripleProbability(&symbTri, &freqTri, &probTri)

	/* table values */
	fmt.Println()
	fmt.Println("Triple symbols that were sorted by probability:")
	i = 0
	for i < len(probTri) {
		fmt.Printf("Probability: %f, Frequancy: %d, Symbol: %c%c%c\n", probTri[i], freqTri[i], symbTri[i].values[0], symbTri[i].values[1], symbTri[i].values[2])
		i++
	}

}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		filename   string
		length     int
		mapSymb    map[int32]int
		lengthBi   int
		mapSymbBi  map[[2]int32]int
		lengthTri  int
		mapSymbTri map[[3]int32]int
		prob       []float64
		symb       []int32
		freq       []int
		probBi     []float64
		symbBi     []BiSymb
		freqBi     []int
		probTri    []float64
		symbTri    []TriSymb
		freqTri    []int
	)

	/* read filename */
	filename = os.Args[1]
	/* calculate symbols in file */
	mapSymb, length, mapSymbBi, lengthBi, mapSymbTri, lengthTri = calculateSymbolsInFile(filename)
	/* make statics about symbols */
	prob, freq, symb, probBi, freqBi, symbBi, probTri, freqTri, symbTri = makeStatics(mapSymb, length, mapSymbBi, lengthBi, mapSymbTri, lengthTri)
	/* write results to user */
	writeResults(prob, freq, symb, length, probBi, freqBi, symbBi, lengthBi, probTri, freqTri, symbTri, lengthTri)

}
